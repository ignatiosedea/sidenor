//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mechanic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Mechanic()
        {
            this.MechanicsDetails = new HashSet<MechanicsDetail>();
        }
    
        public long MechanicsID { get; set; }
        public int AnalysisNo { get; set; }
        public System.DateTime ElasiDate { get; set; }
        public Nullable<short> VardiaEfeklistiriou { get; set; }
        public Nullable<decimal> ReDEPAL { get; set; }
        public Nullable<decimal> RmDEPAL { get; set; }
        public Nullable<decimal> ForceDEPAL { get; set; }
        public string Memo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MechanicsDetail> MechanicsDetails { get; set; }
    }
}
