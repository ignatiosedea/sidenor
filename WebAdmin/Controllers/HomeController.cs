﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Newtonsoft.Json;
using Stomana.Data;
using WebAdmin.Models;
using Newtonsoft.Json.Converters;

namespace WebAdmin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private StomanaAnalysisEntities _dbStomana = new StomanaAnalysisEntities();

        public ActionResult Index()
        {


            var sampeNosDistinct = (from h in _dbStomana.SamplesHeaders
                                    orderby h.DIADateTime descending
                                    select h.SampleNo).ToList().Distinct();

 


            var standards = (from h in _dbStomana.standards
                             
                             select h.quality).Distinct();

            ViewBag.SamplesNo = new SelectList(sampeNosDistinct, "SampleNo");
            ViewBag.Standards = new SelectList(standards, "Standards");

            return View("Index");
        }


        [HttpPost]
        public string HideAnalysis(long id)
        {

            Analysis model = _dbStomana.Analyses.Where(s => s.AnalysisRow == id).FirstOrDefault();

            if (model != null)
            {
                model.visible = false;
                _dbStomana.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _dbStomana.SaveChanges();
                return JsonConvert.SerializeObject(new { msg = "Success" }, Formatting.Indented);
            }

            return JsonConvert.SerializeObject(new { msg = "error" }, Formatting.Indented);
        }



        
        [HttpPost]
        public string ShowAnalysis(long id)
        {

            Analysis model = _dbStomana.Analyses.Where(s => s.AnalysisRow == id).FirstOrDefault();

            if (model != null)
            {
                model.visible = true;
                _dbStomana.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _dbStomana.SaveChanges();
                return JsonConvert.SerializeObject(new { msg = "Success" }, Formatting.Indented);
            }

            return JsonConvert.SerializeObject(new { msg = "error" }, Formatting.Indented);
        }


        [HttpPost]
        public string GetSampleHeadersView(string sample)
        {

            var sampleHeaders = from a in _dbStomana.SamplesHeaders
                                where a.SampleNo == sample
                                orderby a.DIADateTime descending
                                select 
                                new SampleHeadersDTO
                                {                                    
                                    Id = a.Id,
                                    Edit = "",
                                    DIADateTime = a.DIADateTime,
                                    SampleNo = a.SampleNo,
                                    IdentValue = a.IdentValue,
                                    sap_check = a.sap_checked,
                                    visibleAnalysis = a.Analyses.FirstOrDefault().visible
                                };

            return JsonConvert.
                SerializeObject(
                                sampleHeaders.ToList(), Formatting.None, 
                                new JsonSerializerSettings() {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                                DateFormatString = "yyyy-MM-dd HH:mm:ss" }
                                );
         

        }


        [HttpPost]
        public string GetStandardsView(string standard)
        {
            var standards = (from a in _dbStomana.standards
                             where a.quality == standard
                             orderby a.minmax ascending
                             select new
                             {
                                 a.id,
                                 a.quality,
                                 a.minmax,
                                 a.C,
                                 a.Mn,
                                 a.Si,
                                 a.P,
                                 a.S ,
                                 a.Cr,
                                 a.Ni,
                                 a.Cu,                                      
                                 a.Mo,
                                 a.As,
                                 a.Al,                               
                                 a.V,
                                 a.Ti,
                                 a.Nb,
                                 a.Sn,
                                 a.Pb,
                                 a.B,
                                 a.N,
                                 a.Sb,
                                 a.Ca,
                                 a.Al_sol,
                                 a.CEQ
                             }).ToList();

            return JsonConvert.
                            SerializeObject(
                                            standards,
                                            Formatting.Indented, 
                                            new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }
                                            );

        }

        [HttpGet]
        public string GetStandardsFromSample(string sample)
        {
            var standard = _dbStomana.standards.Where(s => s.sampleHeatNo == sample).FirstOrDefault();
            return JsonConvert.SerializeObject(standard, Formatting.Indented);

        }


        [HttpPost]
        [Authorize(Roles = "administrator")]
        public string SapSave(SapPostModel model)
        {
           string message =  "saved to db" + model.id.ToString();
            Stomana.Data.SamplesHeader sampleHeader = _dbStomana.SamplesHeaders.Find(model.id);
            sampleHeader.sap_checked = true;
            _dbStomana.Entry(sampleHeader).State = System.Data.Entity.EntityState.Modified;
            _dbStomana.SaveChanges();
            Analysis analysisRecord = _dbStomana.Analyses.Where(s => s.AnalysisRow == model.id).FirstOrDefault();

            if (analysisRecord != null)
            {

                SAPTable sapTable = new SAPTable()
                {
                    Al = analysisRecord.Al,
                    Al_sol = analysisRecord.Al_sol,
                    AnalysisRow = analysisRecord.AnalysisRow,
                    As = analysisRecord.As,
                    B = analysisRecord.B,
                    C = analysisRecord.C,
                    Ca = analysisRecord.Ca,
                    Ca_sol = analysisRecord.Ca_sol,
                    Ceq = analysisRecord.Ceq,
                    Co = analysisRecord.Co,
                    Con = analysisRecord.Con,
                    Cp = analysisRecord.Cp,
                    Cr = analysisRecord.Cr,
                    Cu = analysisRecord.Cu,
                    Fe = analysisRecord.Fe,
                    Mn = analysisRecord.Mn,
                    Mn_S = analysisRecord.Mn_S,
                    Mo = analysisRecord.Mo,
                    N = analysisRecord.N,
                    Nb = analysisRecord.Nb,
                    Ni = analysisRecord.Ni,
                    P = analysisRecord.P,
                    Pb = analysisRecord.Pb,
                    S = analysisRecord.S,
                    Si = analysisRecord.Si,
                    Sn = analysisRecord.Sn,
                    Ti = analysisRecord.Ti,
                    Tlq = analysisRecord.Tlq,
                    V = analysisRecord.V,
                    visible = analysisRecord.visible,
                    W = analysisRecord.W,
                    DIADateTime = sampleHeader.DIADateTime,
                    IdentValue = sampleHeader.IdentValue,
                    Quality = sampleHeader.Quality,
                    SampleNo = sampleHeader.SampleNo
                };

                _dbStomana.SAPTables.Add(sapTable);
                _dbStomana.SaveChanges();
            }

            return JsonConvert.SerializeObject(new { msg = message }, Formatting.Indented);
        }


        [HttpPost]
        [Authorize(Roles ="administrator")]
        public string AssociateAnalysisToStandar(StandardModel model)
        {

            if (ModelState.IsValid)
            {


                // check if heat sample is save previously and remove it from the standards
                var previousStandards = _dbStomana.standards.Where(s => s.sampleHeatNo == model.sample).ToList();
                foreach( var standard in previousStandards)
                {
                    standard.sampleHeatNo = "";
                    _dbStomana.Entry(standard).State = System.Data.Entity.EntityState.Modified;
                    _dbStomana.SaveChanges();
                }

                if(model.id1 > 0)
                {
                    var standard1 = _dbStomana.standards.Find(model.id1).sampleHeatNo = model.sample;
                    _dbStomana.SaveChanges();
                }

                if (model.id2 > 0)
                {
                    var standard2 = _dbStomana.standards.Find(model.id2).sampleHeatNo = model.sample;
                    _dbStomana.SaveChanges();
                }
                return JsonConvert.SerializeObject(new { msg = "Success" }, Formatting.Indented);

            }

            // return error mesage
             
            return JsonConvert.
                                SerializeObject( 
                                                new  { msg = "Error. Please selct sample and analysis " }, 
                                                Formatting.Indented
                                                );

        }

        public string GetAnalysisView(string sampleno)
        {
            var analysis = (from a in _dbStomana.Analyses
                            join t2 in _dbStomana.SamplesHeaders on a.AnalysisRow equals t2.Id
                            where a.SamplesHeader.SampleNo == sampleno 
                            orderby t2.DIADateTime descending
                            select new AnalysisModel
                            {
                                Heat = t2.SampleNo + ", " + t2.IdentValue,
                                DIADate = t2.DIADateTime,
                                C = a.C,
                                Mn = a.Mn,
                                Si = a.Si,
                                P = a.P,
                                S = a.S,
                                Cr = a.Cr,
                                Ni = a.Ni,
                                Cu = a.Cu,
                                Mo = a.Mo,
                                As = a.As,
                                Al = a.Al,
                                Al_sol = a.Al_sol,
                                Ca_sol = a.Ca_sol,
                                V = a.V,
                                Ti = a.Ti,
                                Nb = a.Nb,
                                Sn = a.Sn,
                                Pb = a.Pb,
                                B = a.B,
                                N = a.N,
                                Ca = a.Ca,
                                TLQ = a.Tlq,
                                CEQ = a.Ceq,
                                Id = a.Id,
                                visible = a.visible
                            }).ToList();

            return JsonConvert.SerializeObject(
                                                analysis,
                                                Formatting.Indented, 
                                                new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }
                                                );
        }
 
        public long calcTlq(Analysis a)
        {
            return 5;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}