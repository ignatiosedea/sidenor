﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Stomana.Data;
using WebAdmin.Models;
using AutoMapper;

namespace WebAdmin.Controllers
{
    [Authorize(Roles ="administrator")]
    public class StandardsController : Controller
    {
        private StomanaAnalysisEntities db = new StomanaAnalysisEntities();

        // GET: Standards
        public ActionResult Index()
        {
            return View(db.standards.ToList());
        }

        // GET: Standards/Details/5
        public ActionResult Details(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            standard standard = db.standards.Find(id);
            if (standard == null)
            {
                return HttpNotFound();
            }
            return View(standard);
        }

        // GET: Standards/Create
        public ActionResult Create()
        {

            //var sampeNosDistinct = (from h in db.SamplesHeaders
            //                        orderby h.SampleNo ascending
            //                        select h.SampleNo).Distinct();

            //ViewBag.SamplesNo = new SelectList(sampeNosDistinct, "SampleNo");

            return View();
        }

        // POST: Standards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StandardModelDTO model)
        {
            if (ModelState.IsValid)
            {
                var minStandard = model.standardMin;
                var maxStandard = model.standardMax;
                Mapper.Initialize(cfg => {
                        cfg.CreateMap<StandardMin, standard>();
                        cfg.CreateMap<StandardMax, standard>();
                    });

                standard minStandardMapped = Mapper.Map<StandardMin, standard>(model.standardMin);
                standard maxStandardMapped = Mapper.Map<StandardMax, standard>(model.standardMax);
                db.standards.Add(minStandardMapped);
                db.standards.Add(maxStandardMapped);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Standards/Edit/5
        public ActionResult Edit(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            standard standard = db.standards.Find(id);
            if (standard == null)
            {
                return HttpNotFound();
            }

            var sampeNosDistinct = (from h in db.SamplesHeaders
                                    orderby h.SampleNo ascending
                                    select h.SampleNo).Distinct();
            ViewBag.SamplesNo = new SelectList(sampeNosDistinct, "SampleNo");

            return View(standard);
        }

        // POST: Standards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "quality,minmax,C,Mn,Si,P,S,Cr,Ni,Cu,Mo,As,Al,Al_sol,Ceq,V,Ti,Nb,Sn,Pb,B,N,Sb,Ca,id")] standard standard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(standard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(standard);
        }

        //// GET: Standards/Delete/5
        //public ActionResult Delete(long id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    standard standard = db.standards.Find(id);
        //    if (standard == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(standard);
        //}

        // POST: Standards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            standard standard = db.standards.Find(id);
            db.standards.Remove(standard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
