﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebAdmin.Models
{
    public class AnalysisModel
    {
        //[JsonProperty(Order =1)]
        //public long Id { get; set; }


        [JsonProperty(Order = 0)]
        public string Heat { get; set; }


        //[JsonProperty(Order = 1)]
        //public bool? visible { get; set; }

        [JsonProperty(Order = 1)]
        public DateTime DIADate { get; set; }

        [JsonProperty(Order = 2)]
        public Nullable<double> C { get; set; }

        [JsonProperty(Order = 3)]
        public Nullable<double> Mn { get; set; }

        [JsonProperty(Order = 4)]
        public Nullable<double> Si { get; set; }

        [JsonProperty(Order = 5)]
        public Nullable<double> P { get; set; }

        [JsonProperty(Order = 6)]
        public Nullable<double> S { get; set; }

        [JsonProperty(Order = 7)]
        public Nullable<double> Cr { get; set; }

        [JsonProperty(Order = 8)]
        public Nullable<double> Ni { get; set; }

        [JsonProperty(Order = 9)]
        public Nullable<double> Cu { get; set; }

        [JsonProperty(Order = 10)]
        public Nullable<double> Mo { get; set; }

        [JsonProperty(Order = 11)]
        public Nullable<double> As { get; set; }

        [JsonProperty(Order = 12)]
        public Nullable<double> Al { get; set; }


        [JsonProperty(Order = 13)]
        public Nullable<double> V { get; set; }

        [JsonProperty(Order = 14)]
        public Nullable<double> Ti { get; set; }

        [JsonProperty(Order = 15)]
        public Nullable<double> Nb { get; set; }

        [JsonProperty(Order = 16)]
        public Nullable<double> Sn { get; set; }

        [JsonProperty(Order = 17)]
        public Nullable<double> Pb { get; set; }

        [JsonProperty(Order = 18)]
        public Nullable<double> B { get; set; }

        [JsonProperty(Order = 19)]
        public Nullable<double> N { get; set; }

        [JsonProperty(Order = 20)]
        public Nullable<double> Ca { get; set; }


        [JsonProperty(Order = 21)]
        public Nullable<double> Al_sol { get; set; }

        [JsonProperty(Order = 22)]
        public double? Ca_sol { get; set; }

        private Nullable<double> Tlq { get; set; }

  
        private Nullable<double> Ceq { get; set; }

        [JsonProperty(Order = 23)]
        public double? TLQ
        {
            get
            {
                return Tlq == null ? 0 : (double)Tlq;
            }
            set
            {
                if (value == null)
                {
                    this.Tlq = 0;
                }

                this.Tlq = LiquidTemperatureEvaluation();                
            }
        }

        [JsonProperty(Order = 24)]
        public double? CEQ
        {
            get
            {
                return Ceq;
            }
            set
            {
                this.Ceq = CarbonEquvalentCalculation();                 
            }
        }


        [JsonProperty(Order = 25)]
        public long Id { get; set; }


        [JsonProperty(Order = 26)]
        public bool? visible { get; set; }

        private double RoundValue(double? value)
        {
            if (value == null)
            {
                return 0;
            }
            IFormatProvider myFormatProvider = new CultureInfo("en-us").NumberFormat;
            var propertyValue = double.Parse(value.ToString(), myFormatProvider);
            return Math.Round(propertyValue, 4);
        }

        /// <summary>
        /// The formula is 1536.6 – (%C x Z) – (%Si x 8 + %Mn x 5 + %P x 30 + %S x 25 + %Cr x 1.5 + %Ni x 4 + %Cu x 5 + %Mo x 2 + %V x 2 + %Al x 5.1)

        /// </summary>
        /// <returns></returns>
        private double LiquidTemperatureEvaluation()
        {
            
            Etable elementTable = new Etable();
            int Z = 0;
            double _C = C == null ? 0 : RoundValue((double)C);

            if (_C > 0.06 && _C <= 0.10)
            {
                Z = 86;
            }
            if (_C >= 0.11 && _C <= 0.50)
            {
                Z = 88;
            }

            if (_C >= 0.51 && _C <= 0.60)
            {
                Z = 86;
            }
            if (_C >= 0.61 && _C <= 0.70)
            {
                Z = 84;
            }
            if (_C >= 0.71 && _C <= 0.80)
            {
                Z = 83;
            }
            if (_C >= 0.81 && _C <= 1.00)
            {
                Z = 82;
            }



            //The formula is 1536.6 – (%C x Z) – (%Si x 8 + %Mn x 5 + %P x 30 + %S x 25 + %Cr x 1.5 + %Ni x 4 + %Cu x 5 + %Mo x 2 + %V x 2 + %Al x 5.1)
            double LT = 1536.6 - (_C * Z) - ( (RoundValue(Si)  * 8)  
                                                + (RoundValue(Mn)  * 5) 
                                                + (RoundValue( P) * 30) 
                                                + (RoundValue(S) * 25) 
                                                + (RoundValue(Cr) * 1.5)
                                                + (RoundValue( Ni) * 4)
                                                + (RoundValue(Cu) * 5)
                                                + (RoundValue(Mo) * 2)
                                                + (RoundValue(V) * 2)
                                                + (RoundValue(Al) * 5.1)
                                                );

            return Convert.ToInt64(LT);
        }



        private class Etable
        {

            public double Si = 8.0;
            public double Mn = 5.0;
            public double P = 30.0;
            public double S = 25.0;
            public double Cr = 1.5;
            public double Ni = 4.0;
            public double Cu = 5.0;
            public double Mo = 2.0;
            public double V = 2.0;
            public double Al = 5.1;

        }

        private double CarbonEquvalentCalculation()
        {
            double _MN = Mn == null ? 0 : RoundValue((double)Mn);
            double _CR = Cr == null ? 0 : RoundValue((double)Cr);
            double _Mo = Mo == null ? 0 : RoundValue((double)Mo);
            double _V = V == null ? 0 : RoundValue((double)V);
            double _Ni = Ni == null ? 0 : RoundValue((double)Ni);
            double _Cu = Cu == null ? 0 : RoundValue((double)Cu);
            double result1 = (RoundValue(_MN / 6));
            var result2 = ((RoundValue(_CR + _Mo + _V)) / 5);
            var result3 = ((RoundValue(_Ni + _Cu)) / 15);
            var result = result1 + result2 + result3;
            double _ceq = RoundValue(RoundValue((double)C) + result);
            return _ceq;
        }
    }
 
}