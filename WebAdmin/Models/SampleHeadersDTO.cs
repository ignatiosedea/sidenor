﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin.Models
{
    public class SampleHeadersDTO
    {
        [JsonProperty(Order = 0)]
        public long Id { get; set; }

        [JsonProperty(Order = 1)]
        public string SampleNo { get; set; }

        [JsonProperty(Order = 2)]
        public String Edit { get; set; }
        

        [JsonProperty(Order = 3)]
        public System.DateTime DIADateTime { get; set; }

        [JsonProperty(Order = 4)]
        public string IdentValue { get; set; }

        [JsonProperty(Order = 4)]
        public bool sap_check { get; set; }

        public bool? visibleAnalysis { get; set; }

    }
}