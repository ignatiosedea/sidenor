﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin.Models
{
    public class StandardModelDTO
    {
        public StandardMax standardMax { get; set; }
        public StandardMin   standardMin { get; set; }

    }

    public  class StandardMin
        {
            public string quality { get; set; }
            public string minmax   { get; set; }

            public decimal C { get; set; }
            public decimal Mn { get; set; }
            public decimal Si { get; set; }
            public decimal P { get; set; }
            public decimal S { get; set; }
            public decimal Cr { get; set; }
            public decimal Ni { get; set; }
            public decimal Cu { get; set; }
            public decimal Mo { get; set; }
            public decimal As { get; set; }
            public decimal Al { get; set; }
            public decimal V { get; set; }
            public decimal Ti { get; set; }
            public decimal Nb { get; set; }
            public decimal Sn { get; set; }
            public decimal Pb { get; set; }
            public decimal B { get; set; }
            public decimal N { get; set; }
            public decimal Sb { get; set; }
            public decimal Ca { get; set; }
            public long id { get; set; }
            public string sampleHeatNo { get; set; }
            public decimal Al_sol { get; set; }
             public decimal Ceq { get; set; }
    }

        public class StandardMax
        {
            public string quality { get; set; }
            public string minmax { get; set; }
            public decimal C { get; set; }
            public decimal Mn { get; set; }
            public decimal Si { get; set; }
            public decimal P { get; set; }
            public decimal S { get; set; }
            public decimal Cr { get; set; }
            public decimal Ni { get; set; }
            public decimal Cu { get; set; }
            public decimal Mo { get; set; }
            public decimal As { get; set; }
            public decimal Al { get; set; }
            public decimal V { get; set; }
            public decimal Ti { get; set; }
            public decimal Nb { get; set; }
            public decimal Sn { get; set; }
            public decimal Pb { get; set; }
            public decimal B { get; set; }
            public decimal N { get; set; }
            public decimal Sb { get; set; }
            public decimal Ca { get; set; }
            public long id { get; set; }
            public string sampleHeatNo { get; set; }
            public decimal Al_sol { get; set; }
            public decimal Ceq { get; set; }
    }

}